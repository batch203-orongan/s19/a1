// console.log("Hello, World!");
// 3. - 5.
let userName;
let password;
let role;

function loginUser(){
	userName = prompt("Enter your username");
	password = prompt("Enter your password");
	role = prompt("Enter your role") //.toLowerCase();

	console.log(userName, password, role);

	if(userName === "" || userName === null){
		return alert("Input must not be empty!");
	}else if(password === "" || password === null){
		return alert("Input must not be empty!");
	}else if(role === "" || role === null){
		return alert("Input must not be empty!");
	}else{
		switch(role){
			case "admin":
				alert("Welcome back to the class portal, admin!");
				break;
			case "teacher":
				alert("Thank you for logging in, teacher!");
				break;
			case "rookie":
				alert("Welcome to the class portal, student!");
				break;
			default:
				alert("Role out of range!");
				break;
		}
	}
}

loginUser();

// 6. 

function averageCalc(num1, num2, num3, num4){
	avg = (num1+num2+num3+num4)/4;
	avg = Math.round(avg);
	console.log("checkAverage ("+num1+", "+num2+", "+num3+", "+num4+")");

	if(avg <= 74){
		console.log("Hello, student, your average is "+avg+". The letter equivalent is F.")
	}
	else if(avg >= 75 && avg <= 79){
		console.log("Hello, student, your average is "+avg+". The letter equivalent is D.")
	}
	else if(avg >= 80 && avg <= 84){
		console.log("Hello, student, your average is "+avg+". The letter equivalent is C.")
	}
	else if(avg >= 85 && avg <= 89){
		console.log("Hello, student, your average is "+avg+". The letter equivalent is B.")
	}
	else if(avg >= 90 && avg <= 95){
		console.log("Hello, student, your average is "+avg+". The letter equivalent is A.")
	}
	else{
		console.log("Hello, student, your average is "+avg+". The letter equivalent is A+.")
	}

}

checkAverage = averageCalc(71, 70, 73, 74);
console.log(checkAverage);

checkAverage = averageCalc(75, 75, 76, 78);
console.log(checkAverage);

checkAverage = averageCalc(80, 81, 82, 78);
console.log(checkAverage);

checkAverage = averageCalc(84, 85, 87, 88);
console.log(checkAverage);

checkAverage = averageCalc(89, 90, 91, 90);
console.log(checkAverage);

checkAverage = averageCalc(91, 96, 97, 95);
console.log(checkAverage);